FROM node
WORKDIR /usr/app
RUN npm install showdown -g
WORKDIR /home/node
ENTRYPOINT [ "/usr/local/bin/showdown" ]
