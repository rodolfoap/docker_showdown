# docker_showdown

Simple packing of https://github.com/showdownjs/showdown Markdown to HTML converter.

Dockerfile: https://gitlab.com/rodolfoap/docker_showdown

### Usage
```
docker run --rm -v $(pwd):/home/node showdown:$TAG makehtml -i SourceFile.md -o TargetFile.html
```
